const express = require('express');
const mongoose = require('mongoose');
const logger = require('morgan');
const bodyParser = require('body-parser');
const passport = require('passport');
const userRoutes = require('./routes/userRoutes');
const vehiculeRoutes = require('./routes/vehiculeRoutes');
const modeleRoutes = require('./routes/modeleRoutes');
const reservationRoutes = require('./routes/reservationRoutes');
const cors = require('cors');
const app = express();
//const Joi = require('joi');

// ----------DB Config ---------//

mongoose.connect(process.env.DATABASE_URL, {useNewUrlParser: true, useUnifiedTopology: true})
const db = mongoose.connection
db.on('error', (error) => console.log(error))
db.once('open', () => console.log('Connected to Database'))

// -------Middlewares-------//

app.use(cors());
app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);

// ----------- Public Images -----------//
app.use(express.static('public'));

// -------Routes-------//
app.use('/api/vehicules', vehiculeRoutes);

app.use('/api/', userRoutes);
app.use('/api/modeles', modeleRoutes);
app.use('/api/reservations', reservationRoutes);

// -------Errors-------//

app.use(( req, res, next) => {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use((err, req, res, next) => {
    const status = err.status || 500;
    const error = err.message || 'Error processing your request';
    res.status(status).send({
        error
    })
});

module.exports = app;

/*app.use(express.json())*/

/*app.post('/api/vehicules', (req, res) => {
    const schema = Joi.object({
        nom: Joi.string()
            .min(3)
            .max(30)
            .required()
    });
    const result = schema.validate( req.body )

    if(result.error) {
        res.status(400).send(result.error.details[0].message);
        return;
    }
    const vehicule = {id: vehicules.length+1, nom: req.body.nom}
    vehicules.push(vehicule)
    res.send(vehicule)
})*/