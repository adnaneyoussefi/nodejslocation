const Modele = require('../models/Modele')

const modeleController = {}

modeleController.get = async (req, res) => {
    try {
        const modeles = await Modele.find()
        res.json( modeles )
    }
    catch(err) {
        res.status(500).json({ message: err.message })
    }
}

modeleController.show = async (req, res) => {
    try {
        let modele = await Modele.findById(req.params.id).populate('vehicules')
        if(modele == null) {
            return res.status(404).json({message: "Cannot find modele"})
        } 
        else
            res.json(modele)
            console.log(req.params.id)
    }
    catch (err) {
        return res.status(500).json({message: err.message})
    }
}

modeleController.create = async (req, res) => {
    const { marque, modele } = req.body
    const modelee = new Modele({
        marque, modele
    })
    try {
        await modelee.save().then(() => res.status(201).json(modelee))
    }
    catch (err) {
        res.status(400).json({ message: err.message })
    }
}

modeleController.update = async (req, res) => {
    const { marque, modele } = req.body
    try {
        let modelee = await Modele.findById(req.params.id)
        if(modelee == null) {
            return res.status(404).json({message: "Cannot find modele"})
        } 
        else {
            const modelee = await Modele.update(
                { _id: req.params.id },
                { marque, modele }
            );
            return res.json(modelee);
        }            
    }
    catch (err) {
        res.status(500).json({ message: err.message })
    }
}

modeleController.destroy = async (req, res) => {
    try {
        let modele = await Modele.findById(req.params.id)
        if(modele == null) {
            return res.status(404).json({message: "Cannot find modele"})
        } 
        else{
            await Modele.deleteOne({_id: req.params.id}).then(() => res.status(204).send({message: "Modele deleted"}))
        }
    }
    catch (err) {
        return res.status(500).json({message: err.message})
    }
}

module.exports = modeleController