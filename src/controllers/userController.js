const jwt = require('jsonwebtoken');
const User = require('../models/User');
const Client = require('../models/Client');
const Admin = require('../models/Admin');
const Joi = require('joi');
const { options } = require('joi');

const userController = {}

//Sign Up
userController.register = async (req, res, next) => {
    const schema = Joi.object({
        nom: Joi.required(), prenom: Joi.required(), adresse: Joi.required(), tel: Joi.required(),
        ville: Joi.required(), email: Joi.required(), password: Joi.required()
    });
    const result = schema.validate( req.body );
    const { nom, prenom, adresse, tel, ville, email, password, joined } = req.body;
    const newUser = new User({
        email, password, joined
    });
    const newClient = new Client({
        nom, prenom, adresse, tel, ville
    });

    if(result.error) {
        res.status(400).send(result.error.details[0].message);
        return;
    }

    try {
        let user = await newUser.save();
        newClient.user = user;
        const client = await newClient.save();
        user = await User.findOneAndUpdate({ _id: user._id }, { client: client._id }, { new: true });
        return res.send({ user , client});
    }catch(e) {
        if(e.code === 11000 && e.name === 'MongoError') {
            let error = new Error(`Email adress ${newUser.email} is already taken`);
            next(error);
        }
        else {
            next(e);   
        }
    }
};

//Login
userController.login = async (req, res, next) => {
    //Username, password in request
    const { email, password } = req.body;
    
    try {
        //Check username and password are ok
        const user = await User.findOne({ email });
        if(!user) {
            const err = new Error(`The email ${email} was not found on our system`);
            err.status = 401;
            next(err);
        }
        user.isPasswordMatch(password, user.password, (err, matched) => {
            if(matched) {
                //if credi ok, then create JWT and return it
                const secret = process.env.JWT_SECRET;
                const expire = process.env.JWT_EXPIRATION;
                
                const token = jwt.sign({ _id: user._id }, secret, { expiresIn: expire });
                return res.send({ token });
                //return res.send({ message: 'you can login' });
            }
            res.status(401).send({
                error: 'Invalid username/password'
                })
        });
    }catch(e) {
        next(e);
    }
    
}

//Sign Up for admin
userController.registerAdmin = async (req, res, next) => {
    const schema = Joi.object({
        nom: Joi.required(), prenom: Joi.required(), adresse: Joi.required(), tel: Joi.required(),
        ville: Joi.required(), email: Joi.required(), password: Joi.required()
    });
    const result = schema.validate( req.body );
    const { nom, prenom, adresse, tel, ville, email, password, joined } = req.body;
    const newUser = new User({
        email, password, joined
    });
    const newAdmin = new Admin({
        nom, prenom, adresse, tel, ville
    });

    if(result.error) {
        res.status(400).send(result.error.details[0].message);
        return;
    }

    try {
        let user = await newUser.save();
        newAdmin.user = user;
        const admin = await newAdmin.save();
        user = await User.findOneAndUpdate({ _id: user._id }, { admin: admin._id }, { new: true });
        return res.send({ user , admin });
    }catch(e) {
        if(e.code === 11000 && e.name === 'MongoError') {
            let error = new Error(`Email adress ${newUser.email} is already taken`);
            next(error);
        }
        else {
            next(e);   
        }
    }
};

userController.me = (req, res, next) => {
    const { user } = req;
    res.send({ user });
};

module.exports = userController;