const Reservation = require('../models/Reservation');
const Vehicule = require('../models/Vehicule');
const Client = require('../models/Client');

const reservationController = {};

reservationController.get = async (req, res) => {
    try {
        const reservations = await Reservation.find().populate('vehicule').populate({
            path: 'client', populate: {path: 'user'}
        });
        res.send(reservations);
    }
    catch(err) {
        res.status(500).json({ message: err.message })
    }
};

reservationController.create = async (req, res) => {
    try {
        const { date_debut, date_fin, vehiculeId } = req.body;
        const reservation = new Reservation({
            date_debut, date_fin, client: req.user.client._id, vehicule: vehiculeId
        });
        await reservation.save();
        const client = await Client.findById(req.user.client._id);
        const vehicule = await Vehicule.findById(vehiculeId);
        client.reservations.push(reservation);
        vehicule.reservation = reservation;
        await client.save();
        await vehicule.save();
        res.status(201).json(reservation);
    } catch(e) {
        res.status(400).json({ message: e.message });
    }
};

reservationController.update = async (req, res) => {
    try {
        let reservation = await Reservation.findById(req.params.id);
        if(reservation == null) {
            return res.status(404).json({message: "Cannot find reservation"});
        }
        else {
            await Reservation.update(
                { _id: req.params.id },
                { livre: reservation.livre == 'false' ? true : false }
            );
            res.status(200).json(reservation);
        }
    } catch(e) {
        res.status(400).json({ message: e.message });
    }
};

reservationController.getByClient = async (req, res) => {
    try {
        const reservations = await Reservation.find({client: req.params.id}).populate('vehicule');
        res.status(200).json(reservations);
    } catch(e) {
        res.status(404).json({ message: e.message });
    }
};

module.exports = reservationController;