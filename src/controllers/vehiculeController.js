const Vehicule = require('../models/Vehicule');
const Modele = require('../models/Modele');
const fs = require('fs');
var moment = require('moment');  

const vehiculeController = {};

const deleteAvatar = (avatarPath) => {
    fs.unlink(avatarPath, (err) => {
        if(err) {
          console.error(err)
          return;
        }
    });
};

vehiculeController.get = async (req, res) => {
    try {
        const vehicules = await Vehicule.find().populate('modele')
        res.send( vehicules )
    }
    catch(err) {
        res.status(500).json({ message: err.message })
    }
}

vehiculeController.chercher = async (req, res) => {
    try {
        const vehicules = await Vehicule.find().populate('reservation');
        let {dateDebut, dateFin} = req.params
        let vehiculesFiltred = vehicules.filter((v) => {
            return (v.reservation && v.reservation.livre) == 'false' || typeof v.reservation == 'undefined' ||
            ((v.reservation && v.reservation.livre) == 'true' && (
            v.reservation && !moment(v.reservation && v.reservation.date_debut).isBetween(moment(dateDebut), moment(dateFin)) &&
            v.reservation && !moment(v.reservation && v.reservation.date_fin).isBetween(moment(dateDebut), moment(dateFin)))
            )
        })
        res.send( vehiculesFiltred )
    }
    catch(err) {
        res.status(500).json({ message: err.message })
    }
}

vehiculeController.show = async (req, res) => {
    try {
        let vehicule = await Vehicule.findById(req.params.id).populate('modele')
        if(vehicule == null) {
            return res.status(404).json({message: "Cannot find vehicule"})
        } 
        else{
            res.json(vehicule)
        }
    }
    catch (err) {
        return res.status(500).json({message: err.message})
    }
}

vehiculeController.create = async (req, res) => {
    const { numSerie, matricule, kilometrage, couleur, carburant, prixJournalier, modele,
        nbrDePersonne, nbrDePorte, boiteVitesse, climatisation } = req.body;
    const { file } = req;
    const vehicule = new Vehicule({
        numSerie, matricule, kilometrage, couleur, carburant, prixJournalier, 
        avatar: file && file.path || null, nbrDePersonne, nbrDePorte, boiteVitesse, climatisation
    });
    try {
        const modeleCible = await Modele.findById(modele);
        vehicule.modele = modeleCible;
        await vehicule.save().then(() => console.log('vehicule added', vehicule));
        modeleCible.vehicules.push(vehicule);
        await modeleCible.save().then(() => console.log('vehicule pushed to modele.vehicules'));
        res.status(201).json(vehicule);
    }
    catch (err) {
        res.status(400).json({ message: err.message });
    }
}

vehiculeController.update = async (req, res) => {
    const { numSerie, matricule, kilometrage, couleur, carburant, prixJournalier, modele,
        nbrDePersonne, nbrDePorte, boiteVitesse, climatisation, avatar } = req.body;
    const { file } = req;
    try {
        let vehicule = await Vehicule.findById(req.params.id);
        if(vehicule == null) {
            return res.status(404).json({message: "Cannot find vehicule"})
        } 
        else {
            const modeleCible = await Modele.findById(modele);
            const perviousModele = await Modele.findById(vehicule.modele._id);
            let index = perviousModele.vehicules.findIndex(v => v == req.params.id);
            perviousModele.vehicules.splice(index, 1);
            await perviousModele.save();
            await Vehicule.update(
                { _id: req.params.id },
                { numSerie, matricule, kilometrage, couleur, carburant, prixJournalier,
                    avatar: file && file.path || avatar, nbrDePersonne, nbrDePorte, boiteVitesse, climatisation,
                    modele: modeleCible }
            );
            modeleCible.vehicules.push(req.params.id);
            await modeleCible.save();
            file && file.path && deleteAvatar(vehicule.avatar);
            return res.json(vehicule);
        }            
    }
    catch (err) {
        res.status(500).json({ message: err.message })
    }
}

vehiculeController.destroy = async (req, res) => {
    try {
        let vehicule = await Vehicule.findById(req.params.id).populate('modele');
        if(vehicule == null) {
            return res.status(404).json({message: "Cannot find vehicule"});
        } 
        else{
            await Vehicule.deleteOne({_id: req.params.id}).then(() => res.status(204).json(vehicule));
            let modele = await Modele.findById(vehicule.modele._id);
            let index = modele.vehicules.findIndex(v => v == req.params.id);
            modele.vehicules.splice(index, 1);
            await modele.save();

            // Supprimer l'avatar du vehicule
            deleteAvatar(vehicule.avatar);
        }
    }
    catch(err) {
        return res.status(500).json({message: err.message});
    }
}

module.exports = vehiculeController;