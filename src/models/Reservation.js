const mongoose = require('mongoose');

const reservationSchema = new mongoose.Schema({
    date_reservation: {
        type: Date,
        required: true,
        default: Date.now
    },
    date_debut: {
        type: Date,
        required: true,
    },
    date_fin: {
        type: Date,
        required: true,
    },
    livre: {
        type: String,
        required: true,
        default: false
    },
    client: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Client',
        required: true
    },
    vehicule: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Vehicule',
        required: true
    }
});

module.exports = mongoose.model('Reservation', reservationSchema);