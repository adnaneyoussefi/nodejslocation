const mongoose = require('mongoose');

const adminSchema = new mongoose.Schema({
    nom: {
        type: String,
        required: true
    },
    prenom: {
        type: String,
        required: true
    },
    adresse: {
        type: String,
        required: true
    },
    tel: {
        type: String,
        required: true
    },
    ville: {
        type: String,
        required: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    vehicules: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Vehicule'
    }],
    clients: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Client'
    }],
});

module.exports = mongoose.model('Admin', adminSchema);