const mongoose = require('mongoose');

const vehiculeSchema = new mongoose.Schema({
    numSerie: {
        type: String,
        required: true
    },
    matricule: {
        type: String,
        required: true
    },
    kilometrage: {
        type: Number,
        required: true
    },
    couleur: {
        type: String,
        required: true
    },
    carburant: {
        type: String,
        required: true
    },
    prixJournalier: {
        type: Number,
        required: true
    },
    climatisation: {
        type: Boolean,
        required: true
    },
    nbrDePersonne: {
        type: Number,
        required: true
    },
    nbrDePorte: {
        type: Number,
        required: true
    },
    boiteVitesse: {
        type: String,
        required: true
    },
    disponibilite: {
        type: Boolean,
        required: true,
        default: true
    },
    avatar: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date.now
    },
    modele: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Modele',
        required: true
    },
    reservation: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Reservation'
    }
});

module.exports = mongoose.model('Vehicule', vehiculeSchema);