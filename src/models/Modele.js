const mongoose = require('mongoose')

const modeleSchema = new mongoose.Schema({
    marque: {
        type: String,
        required: true
    },
    modele: {
        type: String,
        required: true
    },
    vehicules: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Vehicule'
    }]
})

module.exports = mongoose.model('Modele', modeleSchema)