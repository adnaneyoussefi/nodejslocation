const mongoose = require('mongoose');

const clientSchema = new mongoose.Schema({
    nom: {
        type: String,
        required: true
    },
    prenom: {
        type: String,
        required: true
    },
    adresse: {
        type: String,
        required: true
    },
    tel: {
        type: String,
        required: true
    },
    ville: {
        type: String,
        required: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    reservations: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Reservation'
    }],
    admin: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Admin',
    }
});

module.exports = mongoose.model('Client', clientSchema);