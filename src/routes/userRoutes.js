const express = require('express');
const router = express.Router();
const passport = require('passport');
const userController = require('../controllers/userController');

//L'authentification et l'inscription sont public
router.post('/register', userController.register);
router.post('/auth', userController.login);

router.post('/admin/register', userController.registerAdmin);

//Le reste des routes sont protégés (on doit s'authentifier pour les acceder)
router.all('*', (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user) => {
        if(err || !user) {
            const error = new Error('You are not authorized to access this area');
            error.status = 401;
            throw error;
        }

        req.user = user;
        return next();
    })(req, res, next);
})

router.get('/me', userController.me);
router.get('/test', (req, res, next) => {
    return res.send({ message: 'hey, you are authenticated', user: req.user });
});

module.exports = router;