const express = require('express');
const router = express.Router();
const modeleController = require('../controllers/modeleController')

//Read all
router.get('/', modeleController.get)

//Read one
router.get('/:id', modeleController.show)

//Create 
router.post('/', modeleController.create)

//Update
router.put('/:id', modeleController.update)

//Delete
router.delete('/:id', modeleController.destroy)

module.exports = router