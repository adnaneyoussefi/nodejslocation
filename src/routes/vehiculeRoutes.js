const express = require('express');
const router = express.Router();
const vehiculeController = require('../controllers/vehiculeController');
const multer = require('multer');

//Configuration de multer
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public');
    },
    filename: (req, file, cb) => {
        const fileName = `${Date.now()}_${file.originalname.replace(/\s+/g, '-')}`;
        cb(null, fileName);
    },
});
const upload = multer({ storage }).single('avatar');

//Read all
router.get('/', vehiculeController.get);

//Search for available cars
router.get('/chercher/dateDebut/:dateDebut/dateFin/:dateFin', vehiculeController.chercher);

//Read one
router.get('/:id', vehiculeController.show);

//Create 
router.post('/', upload, vehiculeController.create);

//Update
router.put('/:id', upload, vehiculeController.update);

//Delete
router.delete('/:id', vehiculeController.destroy);

module.exports = router;