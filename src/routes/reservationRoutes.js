const express = require('express');
const router = express.Router();
const reservationController = require('../controllers/reservationController');

//Read all
router.get('/', reservationController.get);

//Create 
router.post('/', reservationController.create);

//Update
router.put('/:id', reservationController.update);

//Recuperer les reservations par client 
router.get('/clients/:id', reservationController.getByClient);

module.exports = router;