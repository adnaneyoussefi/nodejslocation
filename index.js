require('dotenv').config();
const app = require('./src/app');

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server is ready for connections on port ${PORT}`));
/*app.post('/api/vehicules', (req, res) => {
    const schema = Joi.object({
        nom: Joi.string()
            .min(3)
            .max(30)
            .required()
    });
    const result = schema.validate( req.body )

    if(result.error) {
        res.status(400).send(result.error.details[0].message);
        return;
    }
    const vehicule = {id: vehicules.length+1, nom: req.body.nom}
    vehicules.push(vehicule)
    res.send(vehicule)
})*/
